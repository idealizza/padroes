# **Guia de desenvolvimento/implementações**

### **0. Observações**
* Existem dois arquivos nesse repositório: um voltado para boas práticas e alguns requisitos para desenvolver qualquer tipo de código e outro com padrões de codificação/sintaxe/estilos. 
* Esse arquivo, README.md, é voltado para boas práticas e segue como um guia do que será exigido/cobrado durante as etapas de revisões de código. 
* O segundo arquivo, PSR_1__2.md, contém respectivamente essas duas PSR's consolidadas, juntamente com alguns exemplos, e que também será cobrado durante a fase de revisões.

## **1. Geral**
* Ao nomear métodos e variáveis, coloque nomes que descrevam de forma clara o seu propósito, não utilize variáveis que
 contém símbolos ou nomes aleatórios
* Utilize PHPDoc em todas as funções que criar - ao menos informe parâmetros de entrada, retornos e descrição 
curta/longa (apenas se for REALMENTE NECESSÁRIO). Isso nos deixará em um caminho mais perto de podermos gerar documentações de código de uma forma mais 
fácil e que nos ajude a dar manutenção no projeto. Sem falar que ajuda todos que usam IDE's com autocomplete pra 
desenvolver - um código bem documentado se explica sozinho.
* Utilize comentários de blocos/linhas sempre que achar necessário uma explicação mais detalhada
* **Evite** comentários redundantes (trechos de códigos muitos simples, comentados)
* Não comente códigos que você deseja que não sejam mais executados, usamos controle de versão também pra tornar o 
processo de obtenção de códigos antigos mais fácil, **evite** lixos no código.
* A documentação do cakephp é sua amiga, utilize-a.
* Existem erros de traduções na documentação do CakePHP, e nem sempre tudo está muito bem explicado em outras línguas. 
Priorize a documentação em inglês. Caso tenha dificuldades, cursinhos de inglês estão ai pra isso, #seajude.
* Utilize route sempre que for necessário deixar suas URL's mais amigáveis.
* **SEMPRE** que criar um recurso que necessite salvar ou ler arquivos em uma determinada pasta, CRIE os mecanismos 
necessários para gerar essa pasta dinamicamente caso não exista, e configure as permissões necessárias de 
leitura/escrita na mesma.
* Permissões padrão para pastas 755, pra arquivos 644.
* **USE** o recurso AUTO INDENTAÇÃO (auto format) para todos os arquivos ficarem dentro do padrão combinado.
* Vai usar algum Plugin ou biblioteca third party? Use o composer
* Mantenha seu gitignore atualizado, não suba coisas desnecessárias para o git.
* **SEMPRE** que criar um novo diretório (por algum motivo), gere um arquivo "empty" dentro dele para que ele possa ser 
commitado pro projeto.
* **SEMPRE** usar try/catch para disparo de e-mails.
* Envio de email, sempre utilizar layout/views - nunca disparar eles crus a partir de nenhuma entidade

 
## **2. Controllers**
* Devem servir apenas como uma “ponte” entre o model e a view. por isso Controllers **NÃO DEVEM** conter regras de 
negócios. Eles devem ser "magros" com actions que simplesmente chamam o model e colocam os retornos ao dispor das 
views. Validações e qualquer outra regra de negócio deve ser implementada dentro do modelo.
* Se um trecho de código seu está se repetindo muito dentro do controller deve ser considerada a ideia de criar uma 
_function_ ou até mesmo um _component_ (caso você utilize a mesma lógica em mais de um controller).
* Não "_echoe_" dados direto do controlador, é bastante tentador um _echo;die;_ mas utilize _respondAs_ quando
 necessário e crie _views_ para retornar as estruturas/formatos necessários. (vide _RequestHandler_)
* **Evite** usar o método _Allow_ do AuthComponent, use com cautela. Na maioria dos casos você precisará criar uma 
permissão separada a nível de banco pra essa nova função que criou no controlador.
* Padrão para upload de arquivos, não utilizar o webroot, nem deixar a URL aberta, sempre tratar.


## **3. Models**
* Os model são responsáveis por persistir os dados no banco e conter as regra de negócios do sistema. Por isso são 
"gordos" (não entenda isso como poder fazer o que quiser nos models).
* Você pode criar models que não tenham uma tabela representativa em seu banco de dados.
* Se existem regras que podem ser compartilhadas entre Models, pode ser que você deva distribuir parte da lógica em 
comportamentos, o melhor caminho seria criar um _Behaviour_ ou adicionar a função chamada no AppModel.
* **Evite** se repetir, caso um trecho de código seja necessário várias vezes, faça dele uma function.
* **Evite** utilizar _callbacks_ de beforeSave e afterSave, crie métodos que preparam os dados antes de inserir ou 
retorná-los, como o callback é algo global do comportamento do model, uma alteração neles pode prejudicar o 
funcionamento da aplicação e render muita dor de cabeça para encontrar e resolver o problema, utilize _callbacks_ com 
moderação e apenas quando você estiver seguro de que o comportamento do callback realmente deve acontecer sempre.
* **SEMPRE** que alterar qualquer função, verifique se o teste escrito precisa de ajustes. Nem sempre ele precisa falhar 
pra necessitar uma adequação, muitas vezes, a depender da complexidade da mudança (ou da falta de complexidade do 
teste), ele pode gerar vários falsos positivos.
* **Evite** métodos genéricos o máximo possível - quanto mais inputs de entrada para divergir tratamentos internos, mais 
aumenta a quantidade de cenários possíveis a serem testados, e logo, aumenta a complexidade da manutenção/teste.
* Prime por manter seus modelo bem modularizado, **evite** funções muito longas, quebre em várias, uma para cada 
finalidade (uma pra cada função DÂHH). Métodos mais simples e diretos são mais fáceis de serem testados
* **Evite** a criação de virtual_fields como atributo padrão da classe, salvo exceções de quando será realmente 
necessário em todas as consultas diretas e indiretas daquela entidade. Quando necessitar de virtual_fields pra uma 
consulta pontual, crie-os, faça a consulta e depois os drope.
* **SEMPRE** faça uso do App::uses no cabeçalho da classe para definir quais outras classes serão usadas dentro dessa 
mesma classe. ( é importante para seu código funcionar independente de onde estiver sendo executado, seja via 
navegador, ou terminal cli)
* Atributos e métodos estáticos possuem um propósito, não os crie deliberadamente
* CONSTANTES devem ser criadas sempre em UPPERCASE
* Utilize constantes sempre que possível para padronizar e melhorar a legibilidade do seu código. Ótimos tipos de 
informação para entrar como constantes: flags, status, tipagens de entidade.... O uso das constantes evita Bad typo 
(erro de digitação) de strings.
* Nem todo modelo precisar extender do AppModel, saiba quando você precisará de fato extender.
* Procure utilizar transactions com try/catch a fim de realizar melhores tratamentos de exceções e rollbacks de suas 
operações
* **Evite** métodos duplicados! Valide se existe algo parecido ou igual ao que você quer, valide se não é melhor alterar 
algo existente do que criar um método novo.

## **4. Testes**
* Para cada método que você criar você deve fazer o teste unitário do mesmo, lembre-se para que um código seja 
testável ele sempre deve retornar algo.
* Ao testar um método criado, explore todos os cenários possíveis que o seu método pode retornar e faça as validações
 em cima destes cenários.
* Use Fixtures para métodos que utilizam retornos do banco de dados para validar se o que tá retornando realmente é o
 esperado.
* Antes de entregar a sua atividade, certifique-se de que todos os seus testes, passaram, e caso você edite um 
método, verifique se o teste do método ainda está passando, caso não esteja faça as correções necessárias.
* Documente seus "test cases", separe-os de maneira lógica. Muitas vezes você vai precisar de vários assertions para 
confirmar um cenário de teste, agrupe-os de maneira que simplifica para quem está lendo e dando manutenção.
* Descreva a finalidade de cada "Test Case" que está sendo operado, não precisa comentar cada assertion.

## **5. Banco de dados**
* Certifique-se que as tabelas criadas por você respeitam as 3 formas normais. As suas tabelas devem estar sempre 
escritas em lowercase, no plural e utilizando “_” como separador (espaço).
* Nem sempre trabalhar com um banco normatizado é a forma ideal, muitas vezes é necessário escapar um pouco para 
conseguir atingir resultados mais performáticos. Em caso de dúvidas, é bom validar com outros mais experientes
* Procurar trabalhar com indices nos campos que são utilizados dentro de cláusulas where
* **Nunca** inserir "DROP TABLE" dentro de queries no Izzdb
* **SEMPRE** fazer uso de "IF NOT EXISTS" em clausulas de CREATE do Izzdb
* Fazer uso dos arquivos "pre-query" (_pq.sql) ao realizar inserts/alters dentro do Izzdb para evitar erros.
* Nas migrations, nunca fazer uso direto de ID's para inserir ou atualizar dados, utilizar sempre subqueries pesquisando pelo 
registro desejado - pois não há como garantir que o ID será o mesmo em todas as instâncias
* Utilizar um timestamp no início dos arquivos de version para poder distinguir a
 ordem de execução das suas queries, por exemplo: YYYMMDDHHMMSS_primeira_query.sql,
  YYYMMDDHHMMSS_segunda_query.sql, dessa forma, você pode agrupar um mesmo timestamp e ordená-lo para evitar erros 
  durante a execução.
* **SEMPRE** inserir dentro do Izzdb DROP de functions/procedures/views/triggers antes das cláusulas CREATE das mesmas.
* Views/Procedures/Functions/Triggers são seus melhores amigos quando precisar tratar de problemas mais complexos ou 
que necessitem de soluções mais performáticas - use com cautela - consulte um médico responsável em caso de dúvidas 
ou enxaquecas com esse processo de estruturação.
* Cuidado com código de estruturas SQL gerados automaticamente por softwares, **NUNCA** especifique "DEFINER" na criação 
de functions/procedures/views/triggers.
* **NUNCA** especifique o database dentro de uma query no Izzdb

## **6. Views**
* Views devem existir somente para imprimir resultados que foram passados do model para o controller e do controller 
para a view, **NUNCA** coloque regras de negócio dentro das suas Views.
  Nas views você deve evitar até mesmo os IFs e ELSEs.
* Se a sua view estiver utilizando IFs ou qualquer estrutura lógica, crie um Helper para consolidar todas as regras de apresentação.
* Se o trecho/bloco de apresentação da view pode ser utilizada em outras views, considere criar Elements para re-utilizar com facilidade.
* Ao criar CRUD onde a tela de Editar e Adicionar são quase idênticas, crie apenas uma view “admin_cadastro”, e faça 
com que o seu controller utilize ela no lugar das outras 2, assim você centraliza a manutenção das views e reduz 
consideravelmente o retrabalho.
* Não utilize CSS inline, utilize arquivos .css do projeto, sempre fale com o designer antes.
* Sempre use os Helpers do Cake para manipular os assets e/ou elementos que ele já possua, exemplo: tags html, imagens, arquivos css, javascript...
* Sempre que tiver um campo de upload de arquivo, deve ser possível visualizar o arquivo atual e também possibilitar excluí-lo 